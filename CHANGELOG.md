# Changelog

## 1.3.0
* Added the ability to hold all scenes on loading
  * In this mode, scenes will wait until all scenes are loaded in the background, instead of activating upon load completion
* Added a path display and tooltip for the scene list
  * Hovering over an element will display the scene's path
* Added a sanity check to prevent scenes from loading unenvenly and causing race conditions
* Improved progress reporting to be more reliable
* Fixed pragma warnings and updated CreateAssetMenu atributes
* Updated UIToolkit APIs for Unity 2022.2 and newer
  

### 1.2.1
* Added missing changelog meta file

## 1.2.0
* Added support for loading scenes sequentially
* Added support for displaying loading progress
* Fixed issue where the editor throws `IndexOutOfRangeException` when displaying active scenes while a scene is loading

## 1.1.0
* Added option to save sets to an existing group
* Modernized editor controls with UI Toolkit
* Fixed issue where the editor breaks on startup
* Fixed issue where loading from a bootstrapper scene would break scene loading

## 1.0

* Initial Release