﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace AdditiveSceneGroups.Editor {
    /// <summary>
    /// Diplays a list of active scenes
    /// </summary>
    public class ActiveScenesWidget : VisualElement {

        public event Action OnScenesRefresh;

        private IEditorSceneManager sceneManager;
        private ActiveScenesData currentScenes;

        private VisualElement labelRoot;
        private VisualElement buttonGroup;
        private ListView sceneList;
        private List<EditorSceneData> editorSceneData;

        public ActiveScenesWidget(IEditorSceneManager sceneManager) {
            this.sceneManager = sceneManager;
            sceneManager.ActiveScenesChanged += UpdateScenes;

            // Create a title label
            var buttonRow = new VisualElement { name = "group-list-title" };
            buttonRow.Add(new Label("Active Scenes") { name = "inspector-heading" });
            buttonRow.Add(new Button(() => { OnScenesRefresh?.Invoke(); }) { name = "button-refresh", text = "Refresh" });
            Add(buttonRow);

            // Add label list
            editorSceneData = new List<EditorSceneData>(8);
            sceneList = new ListView(editorSceneData, 18, () => new Label(), DrawSceneLabel) {
                name = "scene-list",
                selectionType = SelectionType.Single
            };

#if UNITY_2022_2_OR_NEWER
            sceneList.selectionChanged += selection => {
                foreach (var s in selection) {
                    var data = (EditorSceneData)s;
                    var asset = AssetDatabase.LoadAssetAtPath<SceneAsset>(data.path);
                    EditorGUIUtility.PingObject(asset);
                }
            };
#else
            sceneList.onSelectionChange += selection => {
                foreach (var s in selection) {
                    var data = (EditorSceneData)s;
                    var asset = AssetDatabase.LoadAssetAtPath<SceneAsset>(data.path);
                    EditorGUIUtility.PingObject(asset);
                }
            };
#endif
            Add(sceneList);

            buttonGroup = new VisualElement { name = "button-row" };
            buttonGroup.Add(new Button(AddSceneGroup) { name = "scene-button", text = "Save New Group" });
            buttonGroup.Add(new Button(AddToExistingGroup) { name = "scene-button", text = "Save into Existing Group" });
            Add(buttonGroup);
        }

        public void UpdateScenes(ActiveScenesData sceneData) {
            if (currentScenes.GuidHash != sceneData.GuidHash) {
                currentScenes = sceneData;

                editorSceneData.Clear();
                editorSceneData.AddRange(sceneData.Scenes);

#if UNITY_2022_1_OR_NEWER
                sceneList.Rebuild();
#else
                sceneList.Refresh();
#endif

                // Remove and read the save button
                Remove(buttonGroup);
                Add(buttonGroup);
            }
        }

        private void AddSceneGroup() {
            sceneManager.SerializedManifest.Update();
            var arr = sceneManager.SerializedSets;
            arr.InsertArrayElementAtIndex(arr.arraySize);

            var element = arr.GetArrayElementAtIndex(arr.arraySize - 1);
            element.FindPropertyRelative("Name").stringValue = "New Scene Group";

            var sceneArr = element.FindPropertyRelative("Scenes");
            sceneArr.ClearArray();
            for (var i = 0; i < currentScenes.Scenes.Length; ++i) {
                var scene = currentScenes.Scenes[i];
                sceneArr.InsertArrayElementAtIndex(sceneArr.arraySize);

                var data = sceneArr.GetArrayElementAtIndex(sceneArr.arraySize - 1);
                data.FindPropertyRelative("Name").stringValue = scene.name;
                data.FindPropertyRelative("IsMainScene").boolValue = scene.isMainScene;
                data.FindPropertyRelative("Path").stringValue = scene.path;
            }

            sceneManager.SerializedManifest.ApplyModifiedProperties();
            sceneManager.NotifyManifestUpdate();
            sceneManager.Repaint();
        }

        private void AddToExistingGroup() {
            sceneManager.SerializedManifest.Update();
            var arr = sceneManager.SerializedSets;

            var menu = new GenericMenu();
            for (var i = 0; i < arr.arraySize; ++i) {
                var element = arr.GetArrayElementAtIndex(i);
                var name = element.FindPropertyRelative("Name").stringValue;
                menu.AddItem(new GUIContent(name), false, () => {
                    var sceneArr = element.FindPropertyRelative("Scenes");
                    var scenes = currentScenes.Scenes;
                    sceneArr.arraySize = scenes.Length;
                    for (var k = 0; k < scenes.Length; ++k) {
                        var e = sceneArr.GetArrayElementAtIndex(k);
                        var s = scenes[k];
                        e.FindPropertyRelative("Name").stringValue = s.name;
                        e.FindPropertyRelative("IsMainScene").boolValue = s.isMainScene;
                        e.FindPropertyRelative("Path").stringValue = s.path;
                    }

                    sceneManager.SerializedManifest.ApplyModifiedProperties();
                });
            }
            menu.ShowAsContext();
        }

        private void DrawSceneLabel(VisualElement e, int index) {
            var data = editorSceneData[index];
            var label = e as Label;

            label.text = data.isMainScene ? $"{data.name} (Active)" : $"{data.name}";

            if (data.isMainScene) {
                label.AddToClassList("main-scene");
            } else {
                label.RemoveFromClassList("main-scene");
            }
        }
    }
}
