using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.UIElements;

namespace AdditiveSceneGroups.Editor {
    public class SceneListWidget : VisualElement {

        private SceneAsset[] scenes;
        private ScrollView sceneScrollView;
        private List<(string, VisualElement)> map = new();

        public SceneListWidget() {
            // Add a title bar
            var titlebar = new VisualElement { name = "group-list-title" };
            titlebar.AddToClassList("row");
            titlebar.Add(new Label("Scenes in Project") { name = "scene-list-heading" });
            titlebar.Add(new Button(Refresh) { name = "button-refresh", text = "Refresh" });
            this.Add(titlebar);

            // Add a search field & list
            var searchField = new TextField(128, false, false, ' ') { label = "Search" };
            searchField.RegisterValueChangedCallback(FilterSearch);
            this.Add(searchField);

            this.Add(sceneScrollView = new ScrollView(ScrollViewMode.Vertical));

            Refresh();
        }

        public void Refresh() {
            sceneScrollView.contentContainer.Clear();
            map.Clear();

            var guids = AssetDatabase.FindAssets($"t:SceneAsset");
            for (var i = 0; i < guids.Length; ++i) {
                var path = AssetDatabase.GUIDToAssetPath(guids[i]);
                var asset = AssetDatabase.LoadAssetAtPath<SceneAsset>(path);

                if (asset != null) {
                    var row = CreateRow(asset, path);
                    sceneScrollView.Add(row);
                    map.Add((asset.name.ToLowerInvariant(), row));
                }
            }

            sceneScrollView.schedule.Execute(() => {
                sceneScrollView.contentContainer.Sort((VisualElement lhs, VisualElement rhs) => {
                    var leftText = lhs.Q<Label>(classes: "scene-label").text;
                    var rightText = rhs.Q<Label>(classes: "scene-label").text;
                    return leftText.CompareTo(rightText);
                });
            });
        }

        private VisualElement CreateRow(SceneAsset scene, string path) {
            var container = new VisualElement();
            container.AddToClassList("row");
            container.AddToClassList("scene-row");

            var textColumn = new VisualElement();
            textColumn.AddToClassList("scene-text-column");
            textColumn.tooltip = path;
            container.Add(textColumn);

            var sceneLabel = new Label() { text = scene.name };
            sceneLabel.AddToClassList("scene-label");
            textColumn.Add(sceneLabel);

            var pathLabel = new Label() { text = path };
            pathLabel.AddToClassList("path-label");
            textColumn.Add(pathLabel);

            container.Add(new Button(
                () => EditorGUIUtility.PingObject(scene)) {
                name = "button-scene-ping",
                text = "Ping"
            });
            container.Add(new Button(
                () => EditorSceneManager.OpenScene(path, OpenSceneMode.Single)) {
                name = "button-scene-load",
                text = "Load"
            });
            container.Add(new Button(
                () => EditorSceneManager.OpenScene(path, OpenSceneMode.Additive)) {
                name = "button-scene-add",
                text = "Add"
            });

            return container;
        }

        private void FilterSearch(ChangeEvent<string> search) {
            if (string.IsNullOrWhiteSpace(search.newValue)) {
                sceneScrollView.Query<VisualElement>().ForEach(v => {
                    v.RemoveFromClassList("hidden");
                });
                return;
            }

            var value = search.newValue.ToLowerInvariant();
            for (var i = 0; i < map.Count; ++i) {
                var m = map[i];

                if (m.Item1.Contains(value)) {
                    m.Item2.RemoveFromClassList("hidden");
                } else {
                    m.Item2.AddToClassList("hidden");
                }
            }
        }
    }
}