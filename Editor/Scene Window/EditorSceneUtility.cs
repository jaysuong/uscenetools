﻿using System;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace AdditiveSceneGroups.Editor {
    public static class EditorSceneUtility {

        private static List<EditorSceneData> sceneList = new List<EditorSceneData>();

        public static EditorSceneData[] GetLoadedScenes() {
            sceneList.Clear();

            var sceneCount = SceneManager.sceneCount;
            var activeScene = SceneManager.GetActiveScene();

            for (var i = 0; i < sceneCount; ++i) {
                var scene = SceneManager.GetSceneAt(i);
                if (scene.isLoaded) {
                    sceneList.Add(new EditorSceneData {
                        name = scene.name,
                        isMainScene = scene == activeScene,
                        path = scene.path
                    });
                }
            }

            return sceneList.ToArray();
        }

        public static void LoadSceneGroup(SceneSet group) {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();

            var scenes = group.Scenes;

            for (var i = 0; i < scenes.Length; ++i) {
                var scene = scenes[i];
                EditorSceneManager.OpenScene(scene.Path, i == 0 ? OpenSceneMode.Single : OpenSceneMode.Additive);

                if (scene.IsMainScene) {
                    var activeScene = EditorSceneManager.GetSceneByName(scene.Name);
                    EditorSceneManager.SetActiveScene(activeScene);
                }
            }
        }

        private static int CountActiveScenes(SceneSetup[] scenes) {
            var ct = 0;

            for (var i = 0; i < scenes.Length; ++i) {
                if (scenes[i].isLoaded) {
                    ct++;
                }
            }

            return ct;
        }

        public static int CountActiveScenes() {
            var total = SceneManager.sceneCount;
            var ct = 0;

            for (var i = 0; i < total; ++i) {
                var scene = SceneManager.GetSceneAt(i);
                if (scene.isLoaded && !string.IsNullOrEmpty(scene.name) && !string.IsNullOrEmpty(scene.path)) {
                    ct++;
                }
            }

            return ct;
        }

    }
}
