using UnityEngine;
using System.Collections;

namespace AdditiveSceneGroups {

    /// <summary>
    /// Automatically loads a set of scenes to the scene loader
    /// </summary>
    [DisallowMultipleComponent]
    public class SceneBootstrapper : MonoBehaviour {
        public SceneData[] PendingScenes => scenesToLoad;

#pragma warning disable CS0649
        [SerializeField] protected SceneLoader sceneLoader;
        [SerializeField] protected bool autoLoadScenes;
        [SerializeField] protected bool loadFromManifest;
        [SerializeField] protected SceneManifest manifest;
        [SerializeField] protected string setName;
        [SerializeField] protected LoadOptions loadOptions;
        [SerializeField, HideInInspector] protected SceneData[] scenesToLoad;
#pragma warning restore CS0649

        protected enum LoadBehaviorType : byte { OverwriteLoadedScenes, AppendToLoadedScenes }

        protected virtual IEnumerator Start() {
            yield return null;

            if (autoLoadScenes) {
                LoadScenes();
            }
        }

        public virtual void LoadScenes() {
            if (loadFromManifest) {
                sceneLoader.LoadScenes(manifest[setName].Scenes, loadOptions);
            } else {
                sceneLoader.LoadScenes(scenesToLoad, loadOptions);
            }
        }
    }
}
