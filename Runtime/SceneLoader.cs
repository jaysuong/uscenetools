
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AdditiveSceneGroups {

    [Flags]
    public enum LoadOptions : sbyte {
        ClearExistingScenes = 1 << 0,       // Every loaded scene in the pool is unloaded
        ReloadMatchingScenes = 1 << 1,      // For every matching scene in the new set, unload and reload the scene
        KeepMatchingScenes = 1 << 2,        // Every matching scene is preserved
        LoadSequentially = 1 << 3,          // Loads the scenes sequentially instead of queueing them all at once
        HoldLoadedScenes = 1 << 4           // Holds the scenes when loaded to manually activate later
    }

    /// <summary>
    /// A high-level simple scene loading manager that loads batches of scenes in one go
    /// </summary>
    [CreateAssetMenu(menuName = "Scriptable Objects/AdditiveSceneGroups/Scene Loader", fileName = "Scene Loader")]
    public class SceneLoader : ScriptableObject {

        public SceneData[] ActivePool => FetchRunningScenesRuntime();
        public string[] ActiveScenes => FetchSceneNames(ActivePool);
        public SceneLoadingProgress Progress => progress;

        private AsyncOperation[] pendingLoadedScenes = new AsyncOperation[0];
        private SceneLoadingProgress progress = new SceneLoadingProgress();

        private const LoadOptions ForceUnloadOptions = LoadOptions.ClearExistingScenes | LoadOptions.ReloadMatchingScenes;
        private const string GlobalFallbackScene = "Runtime_Fallback_Scene";

        public void LoadScenes(SceneData[] scenes, LoadOptions options, ThreadPriority loadPriority = ThreadPriority.Normal) {
            if (scenes.Length < 1) {
                throw new InvalidOperationException("Cannot load 0 scenes!");
            }

            InsertPlaceholderScene();

            // Reset progress
            progress.Prepare();

            // Compare the scene and the current set to find matches
            var pool = FetchRunningScenesRuntime();
            var toUnload = FetchScenesToUnload(scenes, pool, options);
            UnloadScenes(toUnload, pool, () => {
                // Set progress to loading
                progress.UpdateStage(LoadStage.Loading);

                Application.backgroundLoadingPriority = loadPriority;
                var toLoad = FetchScenesToLoad(scenes, pool, options);
                // Load Pending Scenes
                LoadPendingScenes(
                    toLoad,
                    options,
                    () => {
                        progress.Complete();
                    });
            });
        }

        public bool TryCompleteLoading() {
            var canComplete = true;

            for (var i = 0; i < pendingLoadedScenes.Length; ++i) {
                if (pendingLoadedScenes[i].progress < 0.9f) {
                    canComplete = false;
                    break;
                }
            }

            if (canComplete) {
                for (var i = 0; i < pendingLoadedScenes.Length; ++i) {
                    pendingLoadedScenes[i].allowSceneActivation = true;
                }

                Array.Resize(ref pendingLoadedScenes, 0);
            }

            return canComplete;
        }

        private string[] FetchSceneNames(SceneData[] activeSet) {
            var scenes = new string[activeSet.Length];

            for (var i = 0; i < scenes.Length; ++i) {
                scenes[i] = activeSet[i].Name;
            }

            return scenes;
        }


        private bool Contains(SceneData data, SceneData[] arr) {
            for (var i = 0; i < arr.Length; ++i) {
                if (arr[i].Path == data.Path) {
                    return true;
                }
            }

            return false;
        }

        private SceneData[] FetchRunningScenesRuntime() {
            var scenes = new SceneData[SceneManager.sceneCount];
            for (var i = 0; i < scenes.Length; ++i) {
                var scene = SceneManager.GetSceneAt(i);
                scenes[i] = new SceneData {
                    Name = scene.name,
                    Path = scene.path,
                    IsMainScene = !scene.isSubScene
                };
            }

            return scenes;
        }

        private SceneData[] FetchScenesToLoad(SceneData[] scenes, SceneData[] runningSet, LoadOptions options) {
            var result = new SceneData[scenes.Length];
            var forceUnload = (options & ForceUnloadOptions) != 0;
            var index = 0;

            for (var i = 0; i < scenes.Length; ++i) {
                var s = scenes[i];

                if (!s.IsRuntimeScene && (!Contains(s, runningSet) || forceUnload)) {
                    result[index++] = s;
                }
            }

            Array.Resize(ref result, index);

            return result;
        }

        private SceneData[] FetchScenesToUnload(SceneData[] scenes, SceneData[] runningSet, LoadOptions options) {
            var result = new SceneData[runningSet.Length];
            var forceUnload = (options & ForceUnloadOptions) != 0;
            var index = 0;

            for (var i = 0; i < runningSet.Length; ++i) {
                var r = runningSet[i];

                if (!r.IsRuntimeScene && (!Contains(r, scenes) || forceUnload)) {
                    result[index++] = r;
                }
            }

            Array.Resize(ref result, index);

            return result;
        }

        private void InsertPlaceholderScene() {
            var count = SceneManager.sceneCount;
            for (var i = 0; i < count; ++i) {
                var scene = SceneManager.GetSceneAt(i);
                if (scene.name == GlobalFallbackScene) {
                    return;
                }
            }

            SceneManager.CreateScene(GlobalFallbackScene);
        }

        private void LoadPendingScenes(SceneData[] scenes, LoadOptions options, Action onComplete) {
            if (scenes.Length < 1) {
                onComplete?.Invoke();
                return;
            }

            if ((options & LoadOptions.LoadSequentially) != 0) {
                LoadNextScene(0, scenes, onComplete);
                return;
            }

            if ((options & LoadOptions.HoldLoadedScenes) != 0) {
                Array.Resize(ref pendingLoadedScenes, scenes.Length);
                var total = scenes.Length;

                for (var i = 0; i < scenes.Length; ++i) {
                    var scene = scenes[i];
                    var op = SceneManager.LoadSceneAsync(scene.Name, LoadSceneMode.Additive);
                    op.allowSceneActivation = false;
                    pendingLoadedScenes[i] = op;
                }

                progress.PushOps(pendingLoadedScenes);
            } else {
                var total = scenes.Length;
                var loadCount = 0;

                for (var i = 0; i < scenes.Length; ++i) {
                    var scene = scenes[i];
                    var op = SceneManager.LoadSceneAsync(scene.Name, LoadSceneMode.Additive);
                    op.completed += c => {
                        if (scene.IsMainScene) {
                            SceneManager.SetActiveScene(SceneManager.GetSceneByName(scene.Name));
                        }

                        loadCount++;

                        if (++loadCount >= total) {
                            onComplete?.Invoke();
                        }
                    };
                }

                progress.PushOps(new AsyncOperation[0]);
            }
        }

        private void LoadNextScene(int index, SceneData[] scenes, Action onComplete) {
            if (index >= scenes.Length) {
                onComplete?.Invoke();
                return;
            }

            var scene = scenes[index];
            var op = SceneManager.LoadSceneAsync(scene.Path, LoadSceneMode.Additive);
            op.completed += c => {
                if (scene.IsMainScene) {
                    SceneManager.SetActiveScene(SceneManager.GetSceneByPath(scene.Path));
                }

                LoadNextScene(index + 1, scenes, onComplete);
            };
        }

        private void UnloadScenes(SceneData[] scenes, SceneData[] runningScenes, Action onComplete) {
            if (scenes.Length < 1) {
                onComplete?.Invoke();
                return;
            }

            // Count valid scenes
            var validCount = 0;
            for (var i = 0; i < scenes.Length; ++i) {
                if (Contains(scenes[i], runningScenes)) {
                    validCount++;
                }
            }

            if (validCount == 0) {
                onComplete();
                return;
            }

            var unloadCount = 0;
            var ops = new AsyncOperation[validCount];
            for (var i = 0; i < scenes.Length; ++i) {
                var scene = scenes[i];
                if (Contains(scene, runningScenes)) {
                    var op = SceneManager.UnloadSceneAsync(scene.Name);
                    op.completed += c => {
                        if (++unloadCount >= validCount) {
                            onComplete();
                        }
                    };

                    ops[i] = op;
                }
            }

            progress.PushOps(ops);
        }

    }
}

