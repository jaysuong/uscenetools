using UnityEngine;

namespace AdditiveSceneGroups {
    public enum LoadStage {
        Idle,
        Unload,
        Loading,
        Complete
    }

    public class SceneLoadingProgress {

        public LoadStage Stage => stage;
        public float Progress {
            get {
                switch (stage) {
                    case LoadStage.Idle:
                        return 0f;

                    case LoadStage.Complete:
                        return 1f;

                    default: {
                            if (operations == null || operations.Length < 1) {
                                return 1f;
                            }

                            return CalculateAverageProgress();
                        }
                }
            }
        }

        private LoadStage stage;
        private AsyncOperation[] operations;

        private static readonly AsyncOperation[] EmptyOps = new AsyncOperation[0];

        internal void Complete() {
            stage = LoadStage.Complete;
            operations = EmptyOps;
        }

        internal void Prepare() {
            stage = LoadStage.Unload;
            operations = EmptyOps;
        }

        internal void PushOps(AsyncOperation[] ops) {
            operations = ops;
        }

        internal void UpdateStage(LoadStage stage) {
            this.stage = stage;
        }

        private float CalculateAverageProgress() {
            var total = 0f;
            for (var i = 0; i < operations.Length; i++) {
                total += operations[i].progress;
            }
            return total / operations.Length;
        }
    }
}