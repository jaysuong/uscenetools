using System;
using UnityEngine;

namespace AdditiveSceneGroups {
    [Serializable]
    public struct SceneSet {
        public string Name;
        public SceneData[] Scenes;

        public static SceneSet Empty => new SceneSet { Name = string.Empty, Scenes = new SceneData[0] };
    }

    /// <summary>
    /// A collection of saved scene sets.  Values are editable in the editor
    /// </summary>
    [CreateAssetMenu(menuName = "Scriptable Objects/AdditiveSceneGroups/Scene Manifest", fileName = "Scene Manifest")]
    public class SceneManifest : ScriptableObject {

        public SceneSet[] Sets => sets;
        public int Length => sets.Length;

        public SceneSet this[string name] {
            get {
                for (var i = 0; i < sets.Length; ++i) {
                    if (sets[i].Name == name) {
                        return sets[i];
                    }
                }

                return SceneSet.Empty;
            }
        }

#pragma warning disable 649

        [SerializeField] private SceneSet[] sets;

#pragma warning restore 649

        private void Reset() {
            sets = new SceneSet[0];
        }
    }
}
