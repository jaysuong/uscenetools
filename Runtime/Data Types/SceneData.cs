using System;

namespace AdditiveSceneGroups {
    [Serializable]
    public struct SceneData {
        public string Name;
        public string Path;
        public bool IsMainScene;

        public bool IsRuntimeScene => Name == "Runtime_Fallback_Scene";
    }
}
